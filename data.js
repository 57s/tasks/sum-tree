export const TREE = {
	value: 100,

	left: {
		value: 50,

		left: {
			value: 10,

			left: {
				value: 3,
			},
		},

		right: {
			value: 30,
		},
	},

	right: {
		value: 50,

		right: {
			value: 5,

			right: {
				value: 1,
			},
		},
	},
};
