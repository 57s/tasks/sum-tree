import { TREE } from './data.js';

const sum = ({ left, right, value }) =>
	value + (left ? sum(left) : 0) + (right ? sum(right) : 0);

console.log(sum(TREE));
