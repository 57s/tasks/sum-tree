import { TREE } from './data.js';

function sum({ left, right, value }) {
	if (left && right) {
		return value + sum(left) + sum(right);
	}

	if (left) {
		return value + sum(left);
	}

	if (right) {
		return value + sum(right);
	}

	return value;
}

console.log(sum(TREE));
