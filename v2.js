import { TREE } from './data.js';

function sum({ left, right, value }) {
	let result = value;
	if (left) {
		result += sum(left);
	}

	if (right) {
		result += sum(right);
	}

	return result;
}

console.log(sum(TREE));
